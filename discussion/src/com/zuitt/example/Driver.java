package com.zuitt.example;

public class Driver {
    private String name;

    // Constructor
    public Driver() {

    }

    public Driver(String name) {
        this.name = name;
    }

    //Getter and Setter
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
